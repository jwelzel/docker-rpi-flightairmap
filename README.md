# Docker Container with NGINX + PHP7 + Flightairmap for Raspberry Pi

### ENV Variables
```
FLIGHT_INSTALLED=TRUE
FLIGHT_TIMEZONE=Europe/Berlin
FLIGHT_DB_HOST=mysql
FLIGHT_DB_USER=flight
FLIGHT_DB_PASS=PASSWORD
FLIGHT_DB_NAME=flight
FLIGHT_SBS1_HOST=dump1090-piaware
FLIGHT_SBS1_PORT=30003
FLIGHT_SQUAWK_CO=DE
FLIGHT_CENTER_LAT=XX.XXXXXX
FLIGHT_CENTER_LON=XX.XXXXXX
```

### Thanks
* To Frederik Granna for his [blog post](http://www.sysrun.io/2015/11/20/a-complete-docker-rpi-rtl-sdr-adsbacars-solution/) and some docker/code inspirations
* To Glenn Stewart for his [piaware containers](https://hub.docker.com/u/inodes/) 
