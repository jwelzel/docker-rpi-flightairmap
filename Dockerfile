FROM resin/rpi-raspbian:stretch 

RUN apt-get update && apt-get upgrade && \
    apt-get install -y nginx php7.0 php7.0-fpm php7.0-mysql php7.0-sqlite3 php7.0-curl php7.0-json php7.0-zip \
	php7.0-xml php7.0-sqlite3 mysql-client vim pkg-config ca-certificates git-core inetutils-ping cron  --no-install-recommends && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*


WORKDIR /tmp

RUN git clone --recursive https://github.com/Ysurac/FlightAirMap.git && \
    mv /tmp/FlightAirMap /var/www/flightairmap && \
    cp /var/www/flightairmap/install/init/flightairmap.init /etc/init.d/flightairmap && \
    chmod 755 /etc/init.d/flightairmap && \
    chown -R www-data /var/www/flightairmap

COPY nginx_host.conf /etc/nginx/sites-enabled/nginx_host.conf

RUN rm /etc/nginx/sites-enabled/default

COPY settings.php /var/www/flightairmap/require/settings.php

COPY dbupdate-cron /etc/cron.d/dbupdate

COPY startup.sh /opt/startup.sh

CMD /opt/startup.sh
